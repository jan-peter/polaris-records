		    ________________________________

		     POLARIS MEETING - MAY 28, 2021

			       Hugh Brown
		    ________________________________


Table of Contents
_________________

1. [2021-05-28 Fri]  Polaris meeting
.. 1. Attending
.. 2. Discussion of Ayush's GSoC work
.. 3. Discussion of data format for Betsi/Polaris
.. 4. Opssat
.. 5. Julien: graph for Betsi
.. 6. TODO Red to submit issue w/proposal for his analysis
.. 7. TODO Adithya: Acinonyx InfluxDB
.. 8. TODO Hugh: Keep working on the OPS-SAT proposal
.. 9. TODO Julien: Add notebook to Polaris playground repo


1 [2021-05-28 Fri]  Polaris meeting
===================================

1.1 Attending
~~~~~~~~~~~~~

  - Jan-Peter
  - Ayush
  - Julien
  - Hugh
  - Adithya
  - Red


1.2 Discussion of Ayush's GSoC work
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Red: would be good to discuss a bit what we are building with
    anomaly reports
    - Proposal for further analysis to be done: make connection between
      Betsi analysis & graph.
      - JP: isn't Betsi time-based & Polaris not?
      - Red: yes, but Polaris produces graph for a time interval.  Betsi
        divides up time into behaviour periods; we can generate a
        Polaris graph for each of those time periods & compare them.
      - Julien: I worry we won't have enough data for learn.  What if we
        have a behaviour period of one minute?  That's not a lot of
        data.
        - Red: Good point.
    - Red: what I'd need out of Ayush's work is room to add other
      analyses.  Def. would like to get Ayush's involvement!
    - Hugh: What's the best way to get this discussion rolling?
      - Red: proposal/diagram.  I'll submit issue. Also, this is an
        extension of Ayush's proposal -- not erplacing it.
    - Adithya: Can this be made scalable?  That is, ability to analyze
      different data sizes (5 minutes vs 2 weeks).
      - Red: Betsi can analyze sliding window
    - Julien: First step would be to fully integrate Betsi pipeline into
      Polaris.  At this point we're using notebooks to do this analysis.


1.3 Discussion of data format for Betsi/Polaris
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Adithya: is JSON the format we want?
    - Hugh: Adithya & I have different thoughts on this.  Briefly:
      - Adithya believes we're using JSON almost like a database, and
        that's inefficient.  We have some awfully large JSON files right
        now and a database would be a good fit for that.  InfluxDB is a
        time-series-optimized database, and a good candidate.
      - Hugh believes all that is true, but we may not know enough yet
        about our use cases & requirements.  Would be reluctant to have
        work delayed by having to integrate InfluxDB -- plus, this now
        adds complexity to Polaris (as in, it now requires/uses an
        InfluxDB database and users need to add this somehow).  In the
        meantime, JSON is quite flexible.
    - Julien: Agree with Hugh about not needing InfluxDB now.  Don't
      think we're using JSON as db right now; looking at learn, what it
      needs is pretty small.  Also, we don't know right now what we need
      for Betsi - let's stay flexible.
      - Adithya: For background, one of the proposed formats for betsi
        had a quite explicit/verbose format -- this seems like a really
        good format for Influxdb.  Also, Vinvelivaanilai was built with
        InfluxDB -- can we use this code for other things?
      - Julien: Not saying we shouldn't use InfluxDB in parallel -- but
        later if we want to do bigger process, we can definitely do
        this.
    - Ayush: As first step, JSON will be better; leaves us open for room
      later on.
    - Jan-Peter: No opinion.
    - Hugh: Maybe Adithya can continue integrating this?
      - Adithya: I can do this after exams.
      - Julien: What are use cases?
        - Adithya: Integration with SatNOGS db.  We've talked about this
          for a while.
        - Julien: Oh good point.
        - Hugh: Should talk to Acinonyx
        - Ayush: We should talk to users.  What do they want?


1.4 Opssat
~~~~~~~~~~

  - Hugh: We have 'til the 13th of June now.  This past week has been
    busy, so I haven't been working on it; will be doing that today.
    Some remaining tasks:
    - Rough time estimate for a few steps
    - Red to give background on how our proposal is the best from ML POV
    - Making use of their app framework
    - General tidying...I'll know more once I look at this again.
  - Adithya: do we have access to telemetry?
    - Julien: Yes, we do
  - Hugh: Are we all on the same page re: experiment?
    - Julien: Yes, but would be good to define the steps we're going to
      do.
      - Adithya: We have a rough outline of the experiment.
        - Also: camera will eat up a lot of power 7 heat -- should cause
          some nice & obvious change.


1.5 Julien: graph for Betsi
~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Diving into betsi to understand it more, in preparation for opssat.
    Doing this in Jupyter notebooks.
  - Hugh: This looks awesome! Can you add this to the Polaris playground
    repo?
    - Julien: Sure


1.6 TODO Red to submit issue w/proposal for his analysis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1.7 TODO Adithya: Acinonyx InfluxDB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1.8 TODO Hugh: Keep working on the OPS-SAT proposal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1.9 TODO Julien: Add notebook to Polaris playground repo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
